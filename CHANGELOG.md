# CHANGELOG



## v0.30.5 (2024-04-12)

### Ci

* ci: fixed bec install ([`a954640`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/a9546402f5b2f1a43e1c4e17f977c544c326e5dc))

* ci: fixed twine upload if version did not change ([`d7646e8`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/d7646e835ff5d2c8ea749f3b4e24121d992c1454))

* ci: fixed changelog file ([`deded6f`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/deded6ffaca10369fb1e6cf2629f67ded3ab44b5))

### Fix

* fix: fixed bec_server import ([`434fa36`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/434fa36ca43f8dacd9c4f8fdd7556d77bd0a4b03))

### Refactor

* refactor(device_config): removed outdated config file ([`80a964f`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/80a964fae7203cbfb642980e3f89ed35ad6ff0da))

* refactor(device_config): fixed device schema ([`0f3665c`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/0f3665c32fec2f0f95cc57af81d448eca6978919))

* refactor(device_config): upgraded device configs; closes #56 ([`65c72c9`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/65c72c924847644f80fac768ed35e995a6999404))

### Style

* style: moved isort config to pyproject.toml ([`98d61b1`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/98d61b13e42ec294c2be059029e33021ba6ef3a0))

* style: moved black config to pyproject.toml ([`769a45d`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/769a45d7ff97f5d3bc5de5aa63bd2230654ea9d4))

* style: moved pylint to pyproject.toml ([`fcfe024`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/fcfe0242326c61be9251bd98cf9cf29de499facd))


## v0.30.4 (2024-04-12)

### Ci

* ci: fixed upload of release ([`3c37da8`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/3c37da8f515b2effea0950e3236bb9843b7b7b95))

### Fix

* fix: fixed release upload ([`361dc3a`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/361dc3a182231b458e1893da2e6382b1b17e9d5a))

* fix: upgraded pyproject.toml ([`9d67ace`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/9d67ace30d606caa2aaa919fe8225208c4632c7e))


## v0.30.3 (2024-04-12)

### Build

* build: fixed build ([`88ff3bc`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/88ff3bc0cf3c21d87ba50c24e7d9e2352df751c9))

### Fix

* fix: fixed pyproject.toml ([`2793ca3`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/2793ca3eb0c278f6159b0c6d7fcb121b5c969e12))


## v0.30.2 (2024-04-12)

### Fix

* fix: fixed release update ([`3267514`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/3267514c2055f406277b16f13a13744846e3ba77))


## v0.30.1 (2024-04-12)

### Build

* build: upgraded to sem release 9 ([`0864c0c`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/0864c0c04972a2b12be5ad9d3a53fb1a18a8907d))

### Fix

* fix: fixed release upload ([`abc6aad`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/abc6aad167226fd01e02d51ae4739d4c4688e153))


## v0.30.0 (2024-04-12)

### Build

* build: added black to pyproject ([`eb21600`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/eb2160000a19f89c000caf25a69a79e8249e5bf2))

* build: moved to pyproject.toml ([`6ba2428`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/6ba2428dd8e297c3c2098f9a795bb76595a4f5e7))

### Ci

* ci: updated default BEC branch ([`f287efc`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/f287efc831069d7c09de876ed1bf4dff4bd5908e))

### Feature

* feat: add SimWaveform for 1D waveform simulations ([`bf73bf4`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/bf73bf41c4f209ed251bf21d4b0014d031226a4f))

### Refactor

* refactor(sim): added logger statement to flyer ([`6c45dd6`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/6c45dd6a8b8c76776351289c98990dbc05222f5f))

* refactor: renamed pointID to point_id ([`b746278`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/b74627820a5594dc896b059399703baa4917097a))

### Style

* style(black): skip magic trailing comma ([`b1f3531`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/b1f353139b1ecdcfc266219a7a1a4bf525684bea))

### Unknown

* flomni/check_tracker_signal ([`9c09274`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/9c092740b9b38eac7f1046ae07e0667f91983c87))


## v0.29.2 (2024-04-08)

### Fix

* fix: Adapt to FileWriter refactoring ([`e9c626a`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/e9c626a7c8e5ec1b40d70ad412eff85d7796cba9))

### Unknown

* Update .gitlab-ci.yml file ([`32b6d47`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/32b6d476ca4b0deb0eec75519618c005212cc2dd))


## v0.29.1 (2024-04-06)

### Ci

* ci: added isort to pre-commit and ci ([`36d5cef`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/36d5cef4ef14e5566649834b3afdd1efdbfdfc2d))

### Fix

* fix(utils): fixed scan status message in sim mode ([`c87f6ef`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/c87f6ef63f669d6d1288e3521b80b3e0065bf2f4))

### Refactor

* refactor: applied isort to tomcat rotation motors ([`fd1f8c0`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/fd1f8c0ff58c630051cb67d404c6dd07f3403c5b))

* refactor: fixed formatter ([`1e03114`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/1e031140ed0ae4347a8d16a6a5e8647b48573d96))

* refactor: applied isort to repo ([`284c6c4`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/284c6c47a1db25d7ed840404730b1e97da960c14))

### Unknown

* added fourth channel to signal strength readout ([`321bf0c`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/321bf0c403a77efcbf970ea377b53a59377e38d0))


## v0.29.0 (2024-03-28)

### Feature

* feat: add protocols and rotation base device ([`ddd0b79`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/ddd0b790f8ef3e53966c660c431d2f7a9ceda97c))

### Refactor

* refactor: add set for positioner protocol ([`d844168`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/d844168c1f7f31543ff747bb6f2ef3a2f7f1077e))

* refactor: move protocol and base classes to different directory ([`8b77df8`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/8b77df833f4d389293d14f8e3e54de7b38c9f291))

* refactor: cleanup aerotech, fix packaging for release ([`ce43924`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/ce43924ca1601c409a17855957af6847b75ff261))

### Test

* test: fix tests after merge conflict ([`5f5ec72`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/5f5ec72d02c2cb217ab540e82014d90fe5ef8216))

* test: add test for simulated devices and BECprotocols ([`b34817a`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/b34817acf8ef6e60ef493bc2bb830a3a254e7ced))

* test: add tests for proxies ([`2c43559`](https://gitlab.psi.ch/bec/ophyd_devices/-/commit/2c43559aa8e60950ff95e72772820d784aacaa62))


## v0.28.0 (2024-03-26)
